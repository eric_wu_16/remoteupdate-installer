

. C:\Drivers\Doble\CONFIG\DobleUtilities.ps1

Function Load-IniFile ($inputfile)
{
    [string]   $comment = ";"
    [string]   $header  = "^\s*(?!$($comment))\s*\[\s*(.*[^\s*])\s*]\s*$"
    [string]   $item    = "^\s*(?!$($comment))\s*([^=]*)\s*=\s*(.*)\s*$"
    [hashtable]$ini     = @{}
    Switch -Regex -File $inputfile {
        "$($header)" { $section = ($matches[1] -replace ' ','_'); $ini[$section.Trim()] = @{} }
        "$($item)"   { $name, $value = $matches[1..2]; If (($name -ne $null) -and ($section -ne $null)) { $ini[$section][$name.Trim()] = $value.Trim() } }
    }
    Return $ini
}

Function Delete-AutoLogonReg
{
    # delete auto logon registry	
    Remove-ItemProperty -Path $UserInitRegKey -Name "DefaultPassword" -Force -ErrorAction SilentlyContinue | Out-Null
    Remove-ItemProperty -Path $UserInitRegKey -Name "DefaultUserName" -Force -ErrorAction SilentlyContinue | Out-Null
    Remove-ItemProperty -Path $UserInitRegKey -Name "LastUsedUsername" -Force -ErrorAction SilentlyContinue | Out-Null
    Remove-ItemProperty -Path $UserInitRegKey -Name "AutoLogonSID" -Force -ErrorAction SilentlyContinue | Out-Null
    Remove-ItemProperty -Path $UserInitRegKey -Name "AutoLogonCount" -Force -ErrorAction SilentlyContinue | Out-Null
    Remove-ItemProperty -Path $UserInitRegKey -Name "AutoAdminLogon" -Force -ErrorAction SilentlyContinue | Out-Null
    Set-ItemProperty -Path $UserInitRegKey -Name UserInit -Value "C:\Windows\System32\userinit.exe,"
}

function Check-UpdateStatus
{
   Param(
        [Parameter(Mandatory=$true)]
        [String]$Version,
        [Parameter(Mandatory=$false)]
        [String]$RegDUC = "HKLM:\SOFTWARE\Wow6432Node\Doble\DUC"
        )
        
    $childHives=@()
    $childHives = (Get-ChildItem -Path $RegDUC) 
    if ($childHives)
    {
    $childHives = $childHives.PSChildName
    }
    $RemoteUpdateRegFound = $false
    $RemoteUpdateStatus = "unknown"
    foreach ($child in $childHives)
    {
        if ($child -like "*$version*")
        {
            $RemoteUpdateReg = $RegDUC+"\"+$child
            $RemoteUpdateRegFound = $true
            try
            {
                $RemoteUpdateStatus = (Get-ItemProperty -Path $RemoteUpdateReg -Name Status).Status
            }
            catch
            {
                Write-DobleLog "could not read the remote update status..." 
            }
            break
        }
    }
    return [hashtable]@{RemoteUpdateRegFound = $RemoteUpdateRegFound;RemoteUpdateStatus= $RemoteUpdateStatus}
}

Function Enable-MobileInterfaces
{
       Param(
        [Parameter(Mandatory=$true)]
        [Array[]]$MobileAdapterList
        )
                    
    
    $devcon = "c:\windows\system32\devcon.exe"
    $RawAdapters = Get-WmiObject Win32_PnPEntity 

    try
    {
        foreach ($a in $MobileAdapterList)
        {
            $Adapters = @($RawAdapters | Where-Object {$_.Name -match $a})

            if ($Adapters.Count -ne 0)
            {
                foreach ($Adapter in $Adapters) 
                {
                    $deviceID = $Adapter.DeviceID
                    $classGuid = $Adapter.ClassGuid
                    $class = Get-ItemProperty -path HKLM:\SYSTEM\CurrentControlSet\Control\Class\$classGuid | Select Class
                      
                    if ($class.Class.ToUpper() -eq "NET")
                    {
                    "Enabling $Adapter.Name" | Out-Host
                    $result = . $devcon enable @$deviceID |  select-string -pattern ":";
                    #$result | Out-Host
                    }
                    
                    $wmi = Get-WmiObject -Class Win32_NetworkAdapter -filter "Name LIKE '%Generic Mobile Broadband Adapter%'"
                    if ($wmi)
                    {
                        $wmi.Enable()
                    }
                }
            }
        }
    }
    catch
    {
        "Could not enable Mobile Broadband" | Out-Host
        $_.ScriptStackTrace | Out-Host
    }

    # wait for device to be enabled
    $Status = $false
    $DisabledMobileInterfaces = Get-NetAdapter | Where-Object { ( ($_.InterfaceDescription -match "DW5821e") -or ($_.InterfaceDescription -match "Mobile Broadband") -or ($_.InterfaceDescription -match "$mobile_device")) -and ($_.MacAddress -notin $NullOrEmpty) -and $_.Status -eq "Disabled"}
    Foreach ($Interface in $DisabledMobileInterfaces)
    {
        $interfaceName = $Interface.Name
        $StartTime = $(Get-Date)
        do {
            $ElapsedTime = ($(Get-Date) - $StartTime).TotalSeconds
            $n = Get-NetAdapter | Where-Object { ($_.Name -eq $interfaceName) -and ($_.MacAddress -notin $NullOrEmpty) -and ($_.Status -eq "Up") }

            if ($n -notin $NullOrEmpty)
            {
                #"$interfaceName connected after $ElapsedTime seconds" | Out-Host
                $Status = $true
                break
            }
            else
            {
                #"$interfaceName not connected. Elapsed Time: $ElapsedTime seconds" | Out-Host
                Start-Sleep -Seconds 10
            } 
        } while ($ElapsedTime -lt 120)
    }
    
    return $Status
}


function Move-OSUpdateScript
{
   Param(
        [Parameter(Mandatory=$false)]
        [String]$OSUpdateScriptLocation = "C:\Windows\System32\GroupPolicy\Machine\Scripts\Startup\Scripts\Update-Security.ps1",
        [Parameter(Mandatory=$false)]
        [String]$OSUpdateScriptBackupLocation = "C:\Temp\Update-Security-backup.ps1"
        )
    $i = 0    
    while ($i -lt 10)
    {
        $i++
        try
        {
            if (Test-Path ($OSUpdateScriptLocation))
            {
                Move-Item -Path $OSUpdateScriptLocation -Destination $OSUpdateScriptBackupLocation -Force
            }
            break
        }
        catch 
        {
            Write-DobleLog "error moving $OSUpdateScriptLocation" 
			$errormsg = $Error[0].Exception.Message
			$errormsg |Out-Host
            Start-Sleep -Seconds 120
        }
    }
}

function Restore-OSUpdateScript
{
   Param(
        [Parameter(Mandatory=$false)]
        [String]$OSUpdateScriptLocation = "C:\Windows\System32\GroupPolicy\Machine\Scripts\Startup\Scripts\Update-Security.ps1",
        [Parameter(Mandatory=$false)]
        [String]$OSUpdateScriptBackupLocation = "C:\Temp\Update-Security-backup.ps1"
        )
    $i = 0     
    while ($i -lt 5)
    {
        $i++
        try
        {
            if (Test-Path ($OSUpdateScriptLocation))
            {
                Move-Item -Path $OSUpdateScriptBackupLocation -Destination $OSUpdateScriptLocation -Force
            }
            break
        }
        catch 
        {
            Write-DobleLog "error moving $OSUpdateScriptBackupLocation" 
            Start-Sleep -Seconds 10
        }
    }
}
# Create Log file
if (-not (Test-Path "$LOGPATH"))
{
    New-Item -Path "$LOGPATH" -ItemType Directory -Force -Verbose > $Null
}
    
$MyLogPath = "$LOGPATH\$env:COMPUTERNAME\RemoteUpdatesAutoInstall"

if (-Not (Test-Path "$MyLogPath"))
{
    New-Item -ItemType Directory -Path "$MyLogPath" -Force -Verbose > $Null
}
Start-Transcript -Path "$MyLogPath\RemoteUpdateAutoInstall.log" -Append -IncludeInvocationHeader -Force


# Load settings from ini
$NullOrEmpty    = @("",$Null)

$iniPath = Split-Path -Path "$($MyInvocation.MyCommand.Path)"


$iniFullPath = "$iniPath\RemoteUpdates.ini"
"iniFullPath"| Out-Host
$iniFile = Load-IniFile $iniFullPath
$scriptshash = $iniFile['Scripts']
$totalscripts = $scriptshash.Keys.Count
"There are $totalscripts Remote Update scripts"| Out-Host
$last_script = $scriptshash["script_$totalscripts"]
$last_script = $last_script.Replace("`"","")
$RunasDCC =  $iniFile['Settings']['RunasDCC']
$RunasDobleSRV =  $iniFile['Settings']['RunasDobleSRV']
$ManualUpdates = $iniFile['Settings']['ManualUpdates']
"The following updates will run scripts as DCC" | Out-Host
$RunasDCC = $RunasDCC.Replace("`"","").Split(",")
$RunasDCC | Out-Host
"The following updates will run scripts as DobleSRV" | Out-Host
$RunasDobleSRV = $RunasDobleSRV.Replace("`"","").Split(",") 
$RunasDobleSRV | Out-Host
"The following updates will require manually performing the update" | Out-Host
$ManualUpdates = $ManualUpdates.Replace("`"","").Split(",") 
$ManualUpdates| Out-Host


$PSExecLocation = ".\PSTools\PsExec64.exe"
if ($iniFile['Settings']['PSExec'] -notin $NullOrEmpty)
{
    $PSExecLocation = $iniFile['Settings']['PSExec']
}
$mobile_device = "Mobile Broadband"
if ($iniFile['Settings']['mobile_device'] -notin $NullOrEmpty)
{
    $mobile_device = $iniFile['Settings']['mobile_device']
}

# Skip OS update
$SkipOSUpdates = $False 

if ($iniFile['Settings']['SkipOSUpdates'] -notin $NullOrEmpty)
{
    $SkipOSUpdates = $iniFile['Settings']['SkipOSUpdates']
    $SkipOSUpdates = $SkipOSUpdates.Replace("`"","").ToLower()
    if ($SkipOSUpdates -eq "true")
    {
        $SkipOSUpdates = $True
    }
    else
    {
        $SkipOSUpdates = $False
    }
}

# Move OS update script in GPO
if ($SkipOSUpdates)
{
    Move-OSUpdateScript
}

# Define Registry keys
$RegDUC = "HKLM:\SOFTWARE\Wow6432Node\Doble\DUC"
$UserInitRegKey = "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon"



# Enable mobile device 
$list_of_adapters = @("DW5821e","Mobile Broadband","$mobile_device")

Enable-MobileInterfaces -MobileAdapterList $list_of_adapters

Set-NetFirewallProfile -Enabled True



# Run update scripts
$script_count = 0
while ($script_count -lt $totalscripts )
{
    $script_count = $script_count + 1
    $script_count
    $script = $iniFile['Scripts']["script_$script_count"]
    $script = $script.Replace("`"","")
    Write-DobleLog "--------------------------------------------------------------------------------------------------------------" 
    Write-DobleLog "Remote Update Script : $script" 
    
    if (Test-Path $script)
    {		
        # Get the Remote Update version based on the name of the script file
        $filename = Split-Path -Path $script -Leaf
        $filename -match "v[0-9]+\.[0-9]+\.[0-9]+" | Out-Null
        $RemoteUpdateDir = Split-Path -Path $script
        if($Matches)
        {
            $RemoteUpdateVersion = $Matches.0
            Write-DobleLog "Version : $RemoteUpdateVersion" 
        }
        else
        {
            Write-DobleLog "FATAL ERROR: Couldn't get the remote update version based on script name..." 
            Write-DobleLog "...exiting!" 
            break
        }
        
        # Check the DUC registry for Status
        if (Test-Path $RegDUC)
        {	
            $status = Check-UpdateStatus -Version $RemoteUpdateVersion
            $RemoteUpdateRegFound = $status["RemoteUpdateRegFound"]
            $RemoteUpdateStatus = $status["RemoteUpdateStatus"]
            Write-DobleLog "Remote Update Status : $RemoteUpdateStatus" 
        }
        else
        {
            Write-DobleLog "FATAL ERROR:  Unable to read required registry <<$REGDUC>>" 
            Write-DobleLog "...exiting!" 
            break
        }
        
        # Check if this update requires a manual intervention
        if ($RemoteUpdateVersion -in $ManualUpdates -and ((-not $RemoteUpdateRegFound) -or ($RemoteUpdateStatus -ne "SUCCESS")))
        {
            Write-DobleLog "$RemoteUpdateVersion is a manual update.  Please perform the manual update.  The automated update process will continues once DUC finishes the manual update and reboot."
            break
            
        }
        # Run the remote update script if no Registry or Status isn't 'SUCCESS'
        elseif ((-not $RemoteUpdateRegFound) -or ($RemoteUpdateStatus -ne "SUCCESS"))
        {
            try
            {
                Write-DobleLog "starting remote update script...$script" 

                # Determine if the script should run as DobleSRV or System and set the args acccordingly.  Then start the update script.                
                if ($RemoteUpdateVersion -in $RunasDobleSRV)
                {
						
                    $DUC = Get-ItemProperty -Path $RegDUC
                    $DobleSRV_pw = Decrypt-SecureString -EncryptedString $DUC.DobleSRV
                    Write-DobleLog "running script $RemoteUpdateDir\$filename as DobleSRV" 
					if ($last_script -eq $script)
					{	
						$Args = "-accepteula -w $RemoteUpdateDir -i -h -u $env:COMPUTERNAME\DobleSRV -p $DobleSRV_pw powershell.exe -ExecutionPolicy ByPass -File $RemoteUpdateDir\$filename"
					}
					else
					{
						$Args = "-accepteula -w $RemoteUpdateDir -i -h -u $env:COMPUTERNAME\DobleSRV -p $DobleSRV_pw powershell.exe -ExecutionPolicy ByPass -File $RemoteUpdateDir\$filename -NoInventory"
					}
                }
                elseif ($RemoteUpdateVersion -in $RunasDCC)
                {
                    $DUC = Get-ItemProperty -Path $RegDUC
                    $DCC_pw = Decrypt-SecureString -EncryptedString $DUC.DobleCustomerCare
                    Write-DobleLog "running script $RemoteUpdateDir\$filename as DobleCustomerCare" 
					if ($last_script -eq $script)
					{	
						$Args = "-accepteula -w $RemoteUpdateDir -i -h -u $env:COMPUTERNAME\DobleCustomerCare -p $DCC_pw powershell.exe -ExecutionPolicy ByPass -File $RemoteUpdateDir\$filename"
					}
					else
					{
						$Args = "-accepteula -w $RemoteUpdateDir -i -h -u $env:COMPUTERNAME\DobleCustomerCare -p $DCC_pw powershell.exe -ExecutionPolicy ByPass -File $RemoteUpdateDir\$filename -NoInventory"
					}
                    
                }
                else
                {
                    Write-DobleLog "running script $RemoteUpdateDir\$filename as System" 
					if ($last_script -eq $script)
					{	
						$Args = "-accepteula -w $RemoteUpdateDir -i -s powershell.exe -File $RemoteUpdateDir\$filename"
					}
					else
					{
						$Args = "-accepteula -w $RemoteUpdateDir -i -s powershell.exe -File $RemoteUpdateDir\$filename -NoInventory"
					}
                    
                }
                
                
                # Start the script using PSexec
                $statuscode = Start-Process -FilePath "$PSExecLocation" -ArgumentList $Args -NoNewWindow -Wait
                                
                # This while loop checks if update has completed and update the Auto Logon credentials if Status is 'SUCCESS'
                While ($true)
                {
                    $DUC = Get-ItemProperty -Path $RegDUC
                    $ImageVersion = $DUC.ImageVersion
                    if ($ImageVersion -eq $RemoteUpdateVersion)
                    {
                        try
                        {
                            Start-Sleep -Seconds 10
                            $status = Check-UpdateStatus -Version $RemoteUpdateVersion
                            $RemoteUpdateRegFound = $status["RemoteUpdateRegFound"]
                            $RemoteUpdateStatus = $status["RemoteUpdateStatus"]
                            Write-DobleLog "Remote Update Status : $RemoteUpdateStatus" 
                            
                            if($RemoteUpdateStatus -eq "SUCCESS")
                            {
                                Write-DobleLog "Updating auto logon password" 
                                $DCC_pw = Decrypt-SecureString -EncryptedString $DUC.DobleCustomerCare
                                Set-ItemProperty -Path $UserInitRegKey -Name DefaultPassword -Value $DCC_pw 
                                
                                if (($SkipOSUpdates) -and ($last_script -ne $script))
                                {
                                    Write-DobleLog "moving OS Update Script"    
                                    Move-OSUpdateScript
                                }
                                
                                # delete auto logon registry if this is the last script
                                if ($last_script -eq $script)
                                {		
                                    Write-DobleLog "deleting auto logon registries" 
                                    Delete-AutoLogonReg
                                    
                                    Write-DobleLog "setting customized registry to 1" 
                                    Set-ItemProperty -Path $RegDUC -Name Customized -Value 1
                                    
                                    if ($SkipOSUpdates)
                                    {
                                        Write-DobleLog "restoring OS Update Script"    
                                        Restore-OSUpdateScript
                                    }
                                    
                                }

                                Start-Sleep -Seconds 600
                                break
                            }
                        }
                        catch
                        {
                            Write-DobleLog "error checking remote update status" 
                        }	
                    }
                    else # DUC is not yet at the correct version.  Continue waiting until the script finishes.
                    {
                        Start-Sleep -Seconds 10
                    }
                }
                break # break out of loop to prevent next script from running
            }
            catch
            {
                Write-DobleLog "FATAL ERROR:  Error executing script" 
                break
            }
        }
        else  # This update has already completed in the past so we'll skipp it
        {
            Write-DobleLog "skipping $script" 
    
            # delete auto logon registry if this is the last script
            if ($last_script -eq $script)
            {		
                Write-DobleLog "deleting auto logon registries" 
                Delete-AutoLogonReg
                                    
                Write-DobleLog "setting customized registry to 1" 
                Set-ItemProperty -Path $RegDUC -Name Customized -Value 1
                
                Write-DobleLog "removing $PSExecLocation"
                Remove-Item $PSExecLocation -Force -ErrorAction Stop
                
                if ($SkipOSUpdates)
                {
                    Write-DobleLog "restoring OS Update Script"    
                    Restore-OSUpdateScript
                }
            }
        }
    }
    else  # update script is not found
    {
        Write-DobleLog "FATAL ERROR:  Could not find $script.  Please ensure the script is staged on the DUC.  Then restart the DUC to continue the upgrade process" 
        break
    }
}

$input = Read-Host -Prompt "Press any key to exit"


