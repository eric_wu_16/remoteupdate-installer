. C:\Drivers\Doble\CONFIG\DobleUtilities.ps1
Function Load-IniFile ($inputfile)
{
    [string]   $comment = ";"
    [string]   $header  = "^\s*(?!$($comment))\s*\[\s*(.*[^\s*])\s*]\s*$"
    [string]   $item    = "^\s*(?!$($comment))\s*([^=]*)\s*=\s*(.*)\s*$"
    [hashtable]$ini     = @{}
    Switch -Regex -File $inputfile {
        "$($header)" { $section = ($matches[1] -replace ' ','_'); $ini[$section.Trim()] = @{} }
        "$($item)"   { $name, $value = $matches[1..2]; If (($name -ne $null) -and ($section -ne $null)) { $ini[$section][$name.Trim()] = $value.Trim() } }
    }
    Return $ini
}

function Test-RegistryKeyValue
{
    <#
    .SYNOPSIS
    Tests if a registry value exists.

    .DESCRIPTION
    The usual ways for checking if a registry value exists don't handle when a value simply has an empty or null value.  This function actually checks if a key has a value with a given name.

    .EXAMPLE
    Test-RegistryKeyValue -Path 'hklm:\Software\Carbon\Test' -Name 'Title'

    Returns `True` if `hklm:\Software\Carbon\Test` contains a value named 'Title'.  `False` otherwise.
    #>
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)]
        [string]
        # The path to the registry key where the value should be set.  Will be created if it doesn't exist.
        $Path,

        [Parameter(Mandatory=$true)]
        [string]
        # The name of the value being set.
        $Name
    )

    if( -not (Test-Path -Path $Path -PathType Container) )
    {
        return $false
    }

    $properties = Get-ItemProperty -Path $Path 
    if( -not $properties )
    {
        return $false
    }

    $member = Get-Member -InputObject $properties -Name $Name
    if( $member )
    {
        return $true
    }
    else
    {
        return $false
    }

}
function Move-OSUpdateScript
{
   Param(
        [Parameter(Mandatory=$false)]
        [String]$OSUpdateScriptLocation = "C:\Windows\System32\GroupPolicy\Machine\Scripts\Startup\Scripts\Update-Security.ps1",
        [Parameter(Mandatory=$false)]
        [String]$OSUpdateScriptBackupLocation = "C:\Temp\Update-Security-backup.ps1"
        )
    $i = 0    
    while ($i -lt 10)
    {
        $i++
        try
        {
            if (Test-Path ($OSUpdateScriptLocation))
            {
                Move-Item -Path $OSUpdateScriptLocation -Destination $OSUpdateScriptBackupLocation -Force
            }
            break
        }
        catch 
        {
            Write-DobleLog "error moving $OSUpdateScriptLocation" 
			$errormsg = $Error[0].Exception.Message
			$errormsg |Out-Host
            Start-Sleep -Seconds 120
        }
    }
}

function Restore-OSUpdateScript
{
   Param(
        [Parameter(Mandatory=$false)]
        [String]$OSUpdateScriptLocation = "C:\Windows\System32\GroupPolicy\Machine\Scripts\Startup\Scripts\Update-Security.ps1",
        [Parameter(Mandatory=$false)]
        [String]$OSUpdateScriptBackupLocation = "C:\Temp\Update-Security-backup.ps1"
        )
    $i = 0     
    while ($i -lt 5)
    {
        $i++
        try
        {
            if (Test-Path ($OSUpdateScriptLocation))
            {
                Move-Item -Path $OSUpdateScriptBackupLocation -Destination $OSUpdateScriptLocation -Force
            }
            break
        }
        catch 
        {
            Write-DobleLog "error moving $OSUpdateScriptBackupLocation" 
            Start-Sleep -Seconds 10
        }
    }
}
#Load ini file
$iniPath = Split-Path -Path "$($MyInvocation.MyCommand.Path)"
$iniFullPath = "$iniPath\RemoteUpdates.ini"
"iniFullPath"| Out-Host
$iniFile = Load-IniFile $iniFullPath

# Skip OS update
$SkipOSUpdates = $False 

if ($iniFile['Settings']['SkipOSUpdates'] -notin @($Null, ""))
{
    $SkipOSUpdates = $iniFile['Settings']['SkipOSUpdates']
    $SkipOSUpdates = $SkipOSUpdates.Replace("`"","").ToLower()
    if ($SkipOSUpdates -eq "true")
    {
        $SkipOSUpdates = $True
    }
    else
    {
        $SkipOSUpdates = $False
    }
}

if ($SkipOSUpdates)
{  
    Move-OSUpdateScript
}

# change customized value to 0
$RegDUC = "HKLM:\SOFTWARE\Wow6432Node\Doble\DUC"
$Customized = 0
try
{
    $DUC = Get-ItemProperty -Path $RegDUC
    $Customized = $DUC.Customized
	$DCC_pw = Decrypt-SecureString -EncryptedString $DUC.DobleCustomerCare
}
catch
{
	if ($DUC)
    {
        New-ItemProperty -Path $Global:RegDUC -Name Customized -PropertyType dword -Value 0 -ErrorAction SilentlyContinue | Out-Null
        New-ItemProperty -Path $Global:RegDUC -Name FieldUser -PropertyType string -Value "" -ErrorAction SilentlyContinue | Out-Null
    }
    else
    {
        "FATAL ERROR:  Unable to read required registry <<$REGDUC>>" | Out-Host
        "...exiting!" | Out-Host
        return
    }
}

if ($Customized -ne 0)
{
	$OldValue = (Get-ItemProperty -Path $RegDUC -Name Customized).Customized
	"old - Customized"+" : "+$OldValue | Out-Host
	Set-ItemProperty -Path $RegDUC -Name Customized -Value 0
	$NewValue = (Get-ItemProperty -Path $RegDUC -Name Customized).Customized
	"new - Customized"+" : "+$NewValue | Out-Host	
}

# change the autologon registries and update startup to auto launch the InstallRemoteUpdates.cmd
$UserInitRegKey = "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon"
$RegHashTable = @{
	UserInit 			= "C:\Windows\System32\userinit.exe,C:\Drivers\Doble\RemoteUpdates\InstallRemoteUpdates.cmd";
	DefaultUserName 	= "DobleCustomerCare";
	DefaultPassword 	= "$DCC_pw";
	DefaultDomainName 	= "$env:computername";
	AutoAdminLogon 		= "1"
}
ForEach ($RegEntry in $RegHashTable.keys){
	$RegValue = $RegHashTable[$RegEntry]
	if (Test-RegistryKeyValue -Path $UserInitRegKey -Name $RegEntry)
	{
		$OldValue = (Get-ItemProperty -Path $UserInitRegKey -Name $RegEntry).$RegEntry
		"old - "+$RegEntry+" : "+$OldValue | Out-Host
		Set-ItemProperty -Path $UserInitRegKey -Name $RegEntry -Value $RegValue
	}
	else
	{
		$UserInitRegKey+"\\"+$RegEntry+" doesn't exist creating now" | Out-Host
		New-ItemProperty -Path $UserInitRegKey -Name $RegEntry -PropertyType String -Value $RegValue | Out-Null
	}

	$NewValue = (Get-ItemProperty -Path $UserInitRegKey -Name $RegEntry).$RegEntry
	"new - "+$RegEntry+" : "+$NewValue | Out-Host
}

Restart-Computer
